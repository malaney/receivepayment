var minstache = require('minstache');
var $j = require('jquery');
var numeral = require('numeral');
$j(document).ready(function() {
    // make an array for display list
    var charges = [
              { 'id': 1, 'originalAmount': 50, 'openAmount': 50, 'dueDate': '2013-12-12' },
              { 'id': 2, 'originalAmount': 80, 'openAmount': 20, 'dueDate': '2013-12-18' },
              { 'id': 3, 'originalAmount': 180, 'openAmount': 120, 'dueDate': '2013-12-19' }
          ];

    // define array list into div html tag.
    if (charges.length) {
        $j.each(charges, function(idx, o) {
            $j('#charges > ul.list').append(minstache('<div class="row"><div><input type="checkbox" name="charge{{id}}" id="charge_{{id}}" class="charge"/></div><div>{{originalAmount}}</div><div class="amount">{{openAmount}}</div><div class="date">{{dueDate}}</div><div><input data-amount="{{openAmount}}" type="text" name="cp{{id}}" id="cp{{id}}" class="cp" /></div></div>', o));
        });
    }


    // when click on checkbox than calculate total and credit amount
    $j( "input[type=checkbox]" ).on( "click", function() {
        if ($j(this).is(':checked')) {
            var id = $j(this).attr('id');
            var chargesId = id.split("_");

            $j.each(charges, function(idx, o) {
                if (chargesId[1] == o.id) {
                    var totalVal = parseInt(numeral().unformat($j('#total').html())) + parseInt(numeral().unformat(o.openAmount));
                    var totalCredit = parseInt(numeral().unformat($j('#credit').html())) - parseInt(numeral().unformat(o.openAmount));

                    $j('#cp'+chargesId[1]).val(o.openAmount);
                    $j('#total').text('');
                    $j('#credit').text('');
                    $j('#total').append(numeral(totalVal).format('$0,0.00'));
                    $j('#credit').append(numeral(totalCredit).format('$0,0.00'));
                }
            })
        } else {
            var id = $j(this).attr('id');
            var chargesId = id.split("_");

            var totalVal = parseInt(numeral().unformat($j('#total').html())) - parseInt(numeral().unformat($j('#cp'+chargesId[1]).val()));
            var totalCredit = parseInt(numeral().unformat($j('#credit').html())) + parseInt(numeral().unformat($j('#cp'+chargesId[1]).val()));

            $j('#cp'+chargesId[1]).val('');
            $j('#total').text('');
            $j('#credit').text('');
            $j('#total').append(numeral(totalVal).format('$0,0.00'));
            $j('#credit').append(numeral(totalCredit).format('$0,0.00'));
        }
    });


    // when type amount than apply changes into list,total and credit.
    $j('#amount').blur(function() {
        if (this.value) {

            var v = this.value;
            // regular expression for enter number only into amount field.
            if (!v.match(/^\$?\d*\.?\d*$/)) {
                alert('Please enter a valid amount!');
                $j('#amount').focus();
                return false;
            }

            // if $ sign found than replace with null.
            v = numeral().unformat(v);
            var totalAmount = 0;
            $j('#total').text('');
            $j('#credit').text('');
            $j('.charge').prop('checked', false);
            $j('.cp').val('');
            $j.each(charges, function(idx, o) {
                if (v <= 0) {
                  return 0;
                }
                if (v > o.openAmount) {
                    $j('#cp'+o.id).val(numeral(o.openAmount).format('0.00'));
                    $j('#charge_'+o.id).prop('checked',true);

                    totalAmount += o.openAmount;

                    v = v - o.openAmount;
                } else {
                    $j('#cp'+o.id).val(numeral(v).format('0.00'));
                    $j('#charge_'+o.id).prop('checked',true);
                    totalAmount += v;
                    v = 0;
                }
            });
          $j('#total').append(numeral(totalAmount).format('$0,0.00'));
          $j('#credit').append(numeral(v).format('$0,0.00'));
        }
    });

    // clear all applied payments.
    $j( "#clear_payment" ).click(function() {
        $j('.cp').val('');
        $j('.charge').prop('checked',false);
        $j('#total').text('');
        $j('#credit').text('');
    });

    // auto apply payments
    $j( "#auto_apply_payment" ).click(function() {
        // call trigger on clear payment for clear everything.
        $j("#clear_payment").trigger('click');
        $j('#amount').trigger('blur');
    });
});
